﻿using UnityEngine;
using System.Collections;

public enum InputMethod
{
    KeyboardInput,
    MouseInput,
    TouchInput,
    SensorInput
}

public class PlayerInputManager : MonoBehaviour
{
    public bool isActive;
    public InputMethod inputType = InputMethod.KeyboardInput;

    #region AccelerometerParameter
    [SerializeField] float offsetAccelerometer;
    bool isRedChanged, isBlueChanged;
    #endregion

    #region MicrophoneParameter
    [SerializeField] AudioSource audioSource;
    [SerializeField] float threshold = 1;
    float sensitivity = 100;
    float loudness = 0;
    #endregion

    private void Start()
    {
        InitProximity();
        InitMicrophone();
    }

    void Update()
    {
        if (isActive)
        {
            if (inputType == InputMethod.KeyboardInput)
                KeyboardInput();
            else if (inputType == InputMethod.TouchInput)
                TouchInput();
            else if (inputType == InputMethod.MouseInput)
                MouseInput();
            else if (inputType == InputMethod.SensorInput)
                SensorInput();    
        }
    }

    #region KEYBOARD
    void KeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            Managers.Game.red.ChangeLane();
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            Managers.Game.blue.ChangeLane();

    }
    #endregion

    #region TOUCH
    void TouchInput()
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Ended)
            {
                if (touch.position.x < Screen.width / 2)
                    Managers.Game.red.ChangeLane();
                else
                    Managers.Game.blue.ChangeLane();
            }
        }
    }
    #endregion

    #region MOUSE
    void MouseInput()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Input.mousePosition.x < Screen.width / 2)
                Managers.Game.red.ChangeLane();
            else
                Managers.Game.blue.ChangeLane();
        }

    }
    #endregion

    #region SENSOR
    void InitProximity()
    {
        PAProximity.onProximityChange += ProximityInput;
    }

    void InitMicrophone()
    {
        audioSource.clip = Microphone.Start(null, true, 10, 44100);
        audioSource.loop = true;
        audioSource.mute = true;
        if (Microphone.IsRecording(Microphone.devices[0]))
        {
            while (!(Microphone.GetPosition(Microphone.devices[0]) > 0)) { }
            Debug.Log(Microphone.devices[0]);
            audioSource.Play();
        }
        else
        {
            Debug.Log(Microphone.devices[0] + "doesn't work");
        }
    }
    void SensorInput()
    {
        float valuex = Input.acceleration.x;
        float valuey = Input.acceleration.y;
        
        if(valuex > valuey)
        {
            if(valuex > offsetAccelerometer)
                Managers.Game.blue.ChangeLane();
        }
        else
        {
            if(valuey > offsetAccelerometer)
            Managers.Game.red.ChangeLane();
        }

        //MicrophoneInput();
    }

    void ProximityInput(PAProximity.Proximity arg)
    {
        if (inputType == InputMethod.SensorInput)
        {
            if (arg == PAProximity.Proximity.NEAR)
            {
                float temp = Managers.Difficulty.obstacleSpeed;
                Managers.Difficulty.obstacleSpeed = Managers.Difficulty.turboSpeed + temp;
				Managers.IsBoosting = true;
            }
            else
            {
                Managers.Difficulty.obstacleSpeed = Managers.Difficulty.normalSpeed;
				Managers.IsBoosting = false;
            }
        }
    }

    void MicrophoneInput()
    {
        loudness = GetAverageVolume() * sensitivity;
        float l = loudness;
        if(l > threshold)
        {
            Debug.Log("Loudness : " + loudness);
            Managers.Difficulty.obstacleSpeed = Managers.Difficulty.normalSpeed;
        }
        else
        {
            Debug.Log("Loudness : " + loudness);
            Managers.Difficulty.obstacleSpeed -= Time.deltaTime*1;
        }
    }

    float GetAverageVolume()
    {
        float[] data = new float[256];
        float a = 0;
        audioSource.GetOutputData(data, 0);
        foreach(float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a;
    }
    #endregion
}
