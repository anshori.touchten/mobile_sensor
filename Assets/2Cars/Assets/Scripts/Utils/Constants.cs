﻿using UnityEngine;
using System.Collections;

public static class Constants
{
    public static string GAME_NAME = "2Cars";
    public static string ASSETSTORE_URL = "http://u3d.as/C4z";
    public static string FACEBOOK_URL = "https://www.facebook.com/Skard-Games-1268859439806011/";
    public static string TWITTER_URL = "https://twitter.com/skardgame";
    public static string CONTACT_URL = "https://baturalpgurdin.github.io/#/contact";

    public static float LL = -4f;
    public static float LR = -1.31f;
    public static float RL = 1.31f;
    public static float RR = 4f;
}
